import React from 'react';
import { Alert, Button, Card, Col, ListGroup, Row } from 'react-bootstrap';
import { Link, RouteComponentProps } from 'react-router-dom';

import { useAppSelector } from '../app/hooks';
import { countItemsSelector, itemsSelector } from '../app/slices/cartSlice';
import CartItem from '../components/CartItem';
import { currencyFormatter } from '../utils/format';

function CartScreen({ history }: RouteComponentProps) {
  const cartItems = useAppSelector(itemsSelector);
  const countItems = useAppSelector(countItemsSelector);
  const subtotal = cartItems.reduce(
    (acc, item) => acc + item.quantity * item.price,
    0,
  );

  function checkoutHandler() {
    history.push('/shipping');
  }

  if (cartItems.length === 0)
    return (
      <Row>
        <Col>
          <h1>Shopping cart</h1>
          <Alert variant="info">
            Your cart is empty, <Link to="/">go back to the store</Link>
          </Alert>
        </Col>
      </Row>
    );

  return (
    <>
      <Row>
        <Col>
          <h1>Shopping cart</h1>
        </Col>
      </Row>
      <Row>
        <Col md={8}>
          <ListGroup variant="flush">
            {cartItems.map((item) => (
              <CartItem item={item} key={item.product} />
            ))}
          </ListGroup>
        </Col>

        <Col md={4}>
          <Card>
            <ListGroup variant="flush">
              <ListGroup.Item>
                <h2>Subtotal ({countItems}) items</h2>
                <strong>{currencyFormatter.format(subtotal)}</strong>
              </ListGroup.Item>

              <ListGroup.Item>
                <Button
                  type="button"
                  className="btn-block"
                  disabled={cartItems.length === 0}
                  onClick={checkoutHandler}
                >
                  Proceed To Checkout
                </Button>
              </ListGroup.Item>
            </ListGroup>
          </Card>
        </Col>
      </Row>
    </>
  );
}

export default CartScreen;
