import React, { useEffect, useState } from 'react';
import { Button, Col, Form } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router-dom';

import { useAppDispatch, useAppSelector } from '../app/hooks';
import { setPaymentMethod } from '../app/slices/cartSlice';
import CheckoutSteps from '../components/CheckoutSteps';
import FormContainer from '../components/FormContainer';

function PaymentScreen({ history }: RouteComponentProps) {
  const dispatch = useAppDispatch();
  const shippingAddress = useAppSelector((state) => state.cart.shippingAddress);
  const [method, setMethod] = useState<string>();

  useEffect(() => {
    if (!shippingAddress) history.push('/shipping');
  }, [shippingAddress, history]);

  return (
    <FormContainer>
      <CheckoutSteps />

      <h1>Payment</h1>

      <Form
        onSubmit={(event) => {
          event.preventDefault();

          if (method) {
            dispatch(setPaymentMethod(method));
            history.push('/placeorder');
          }
        }}
        onReset={() => {
          setMethod(undefined);
        }}
      >
        <Form.Group as="fieldset">
          <Form.Label as="legend">Select payment method</Form.Label>
          <Col>
            <Form.Check
              type="radio"
              label="PayPal or credit card"
              name="method"
              id="paypal"
              value="paypal"
              required
              onClick={(event) => {
                setMethod(event.currentTarget.value);
              }}
            />
          </Col>
        </Form.Group>

        <Form.Group>
          <Button type="submit" variant="primary">
            Continue
          </Button>
          {Boolean(method) && (
            <Button type="reset" variant="secondary">
              Clear
            </Button>
          )}
        </Form.Group>
      </Form>
    </FormContainer>
  );
}

export default PaymentScreen;
