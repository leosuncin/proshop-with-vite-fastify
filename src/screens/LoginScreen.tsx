import React from 'react';
import { Alert, Button, Col, Form, Row } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { Link, RouteComponentProps } from 'react-router-dom';

import { useAppDispatch, useAppSelector } from '../app/hooks';
import { LoginBody, login } from '../app/slices/authSlice';
import FormContainer from '../components/FormContainer';
import Loader from '../components/Loader';

export const validations = {
  email: {
    required: 'Email should not be empty',
    pattern: {
      message: 'Email should be a valid address',
      value:
        /^[\w.!#$%&'*+\\/=?^`{|}~-]+@[a-zA-Z\d](?:[a-zA-Z\d-]{0,61}[a-zA-Z\d])?(?:\.[a-zA-Z\d](?:[a-zA-Z\d-]{0,61}[a-zA-Z\d])?)*$/,
    },
  },
  password: {
    required: 'Password should not be empty',
    minLength: {
      message: 'Password too short (at least 8 characters required)',
      value: 8,
    },
  },
};

function LoginScreen({ location }: RouteComponentProps) {
  const { handleSubmit, register, formState } = useForm<LoginBody>();
  const dispatch = useAppDispatch();
  const errorMessage = useAppSelector((state) => state.auth.error);
  const search = new URLSearchParams(location.search);
  const redirect = search.get('redirect') ?? '/';

  return (
    <FormContainer>
      <h1>Sign In</h1>
      {errorMessage && <Alert variant="danger">{errorMessage}</Alert>}
      {formState.isSubmitting && <Loader />}
      <Form onSubmit={handleSubmit((data) => dispatch(login(data)))}>
        <Form.Group controlId="login-email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            autoFocus
            {...register('email', validations.email)}
          />
          {formState.errors.email && (
            <Form.Text className="text-warning">
              {formState.errors.email.message}
            </Form.Text>
          )}
        </Form.Group>

        <Form.Group controlId="login-password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Enter password"
            autoComplete="current-password"
            {...register('password', validations.password)}
          />
          {formState.errors.password && (
            <Form.Text className="text-warning">
              {formState.errors.password.message}
            </Form.Text>
          )}
        </Form.Group>

        <Button
          type="submit"
          variant="primary"
          disabled={formState.isSubmitting}
        >
          Sign In
        </Button>
      </Form>

      <Row className="py-3">
        <Col>
          New Customer?{' '}
          <Link to={redirect ? `/register?redirect=${redirect}` : '/register'}>
            Register
          </Link>
        </Col>
      </Row>
    </FormContainer>
  );
}

export default LoginScreen;
