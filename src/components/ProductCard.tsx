import React from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import type { Product } from '../products';
import { currencyFormatter } from '../utils/format';
import Rating from './Rating';

export type ProductCardProps = {
  product: Product;
};

function ProductCard({ product }: ProductCardProps) {
  return (
    <Card className="my-3 p-3 rounded">
      <Link to={`/product/${product._id}`}>
        <Card.Img src={product.image} variant="top" />
      </Link>

      <Card.Body>
        <Link to={`/product/${product._id}`}>
          <Card.Title
            as="h1"
            className="h6 text-wrap"
            style={{ maxHeight: '2rem' }}
          >
            <strong>{product.name}</strong>
          </Card.Title>
        </Link>

        <Card.Text as="div" className="my-3">
          <Rating
            value={product.rating}
            text={`${product.numReviews} reviews`}
          />
        </Card.Text>

        <Card.Text className="h3 my-1">
          {currencyFormatter.format(product.price)}
        </Card.Text>
      </Card.Body>
    </Card>
  );
}

export default ProductCard;
