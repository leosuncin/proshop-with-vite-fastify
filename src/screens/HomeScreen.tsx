import React from 'react';
import { Alert, Col, Row } from 'react-bootstrap';

import { useGetProductsQuery } from '../app/services/productsApi';
import Loader from '../components/Loader';
import ProductCard from '../components/ProductCard';

function HomeScreen() {
  const { data: products, isLoading, error } = useGetProductsQuery();

  if (isLoading) return <Loader />;

  if (error && 'message' in error)
    return <Alert variant="danger">{error.message}</Alert>;

  return (
    <>
      <h1>Latest Products</h1>
      <Row>
        {products?.map((product) => (
          <Col key={product._id} sm={12} md={6} lg={4} xl={3}>
            <ProductCard product={product} />
          </Col>
        ))}
      </Row>
    </>
  );
}

export default HomeScreen;
