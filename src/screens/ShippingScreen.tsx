import React from 'react';
import { Button, Form } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import type { RouteComponentProps } from 'react-router-dom';

import { useAppDispatch, useAppSelector } from '../app/hooks';
import { ShippingAddress, setShippingAddress } from '../app/slices/cartSlice';
import CheckoutSteps from '../components/CheckoutSteps';
import FormContainer from '../components/FormContainer';

export const validations = {
  address: {
    required: 'Address should not be empty',
  },
  city: {
    required: 'City should not be empty',
  },
  postalCode: {
    required: 'Postal code should not be empty',
  },
  country: {
    required: 'Country should not be empty',
  },
};

function ShippingScreen({ history }: RouteComponentProps) {
  const dispatch = useAppDispatch();
  const shippingAddress = useAppSelector((state) => state.cart.shippingAddress);
  const { handleSubmit, register, formState, reset } = useForm<ShippingAddress>(
    { defaultValues: shippingAddress },
  );

  return (
    <FormContainer>
      <CheckoutSteps />

      <h1>Shipping</h1>

      <Form
        onSubmit={handleSubmit((data) => {
          dispatch(setShippingAddress(data));
          history.push('/payment');
        })}
        onReset={(event) => {
          event.preventDefault();
          reset();
        }}
      >
        <Form.Group controlId="shipping-address">
          <Form.Label>Address</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter address"
            autoComplete="address-line1"
            {...register('address', validations.address)}
          />
          {formState.errors.address && (
            <Form.Text className="text-warning">
              {formState.errors.address.message}
            </Form.Text>
          )}
        </Form.Group>

        <Form.Group controlId="shipping-city">
          <Form.Label>City</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter city"
            {...register('city', validations.city)}
          />
          {formState.errors.city && (
            <Form.Text className="text-warning">
              {formState.errors.city.message}
            </Form.Text>
          )}
        </Form.Group>

        <Form.Group controlId="shipping-postalCode">
          <Form.Label>Postal Code</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter postal code"
            {...register('postalCode', validations.postalCode)}
          />
          {formState.errors.postalCode && (
            <Form.Text className="text-warning">
              {formState.errors.postalCode.message}
            </Form.Text>
          )}
        </Form.Group>

        <Form.Group controlId="shipping-country">
          <Form.Label>Country</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter country"
            {...register('country', validations.country)}
          />
          {formState.errors.country && (
            <Form.Text className="text-warning">
              {formState.errors.country.message}
            </Form.Text>
          )}
        </Form.Group>

        <Form.Group>
          <Button type="submit" variant="primary">
            Continue
          </Button>
          <Button
            type="reset"
            variant="secondary"
            style={{ marginLeft: '0.5rem' }}
          >
            Clear
          </Button>
        </Form.Group>
      </Form>
    </FormContainer>
  );
}

export default ShippingScreen;
