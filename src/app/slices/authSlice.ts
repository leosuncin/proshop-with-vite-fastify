import {
  PayloadAction,
  SerializedError,
  createAsyncThunk,
  createSlice,
} from '@reduxjs/toolkit';
import { replace } from 'connected-react-router';

import type { AppDispatch, AppState } from '../store';

export interface User {
  _id: string;
  name: string;
  email: string;
  isAdmin: boolean;
}

export interface LoginBody {
  email: string;
  password: string;
}

export interface RegisterBody {
  name: string;
  email: string;
  password: string;
}

export type ProfileBody = Partial<RegisterBody>;

interface UserWithToken extends User {
  token: string;
}

interface ErrorResponse {
  message: string;
  stack: any | null;
}

export interface AuthState {
  user?: User;
  token?: string;
  error?: string;
}

const initialState: AuthState = {};

export const login = createAsyncThunk<
  UserWithToken,
  LoginBody,
  {
    rejectValue: string;
    dispatch: AppDispatch;
    state: AppState;
  }
>('auth/login', async (payload, { rejectWithValue, dispatch, getState }) => {
  const response = await fetch(`${import.meta.env.VITE_API_URL}/users/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  });

  if (response.status === 401) {
    const error = (await response.json()) as ErrorResponse;

    return rejectWithValue(error.message);
  }

  if (response.ok) {
    const redirect = getState().router.location.query.redirect ?? '/';
    dispatch(replace(redirect));
  }

  return response.json();
});

export const register = createAsyncThunk<
  UserWithToken,
  RegisterBody,
  {
    rejectValue: string;
    dispatch: AppDispatch;
    state: AppState;
  }
>('auth/register', async (payload, { rejectWithValue, dispatch, getState }) => {
  const response = await fetch(`${import.meta.env.VITE_API_URL}/users`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  });

  if (response.status === 401) {
    const error = (await response.json()) as ErrorResponse;

    return rejectWithValue(error.message);
  }

  if (response.ok) {
    const redirect = getState().router.location.query.redirect ?? '/';
    dispatch(replace(redirect));
  }

  return response.json();
});

export const updateProfile = createAsyncThunk<
  UserWithToken,
  ProfileBody,
  {
    rejectValue: string;
    dispatch: AppDispatch;
    state: AppState;
  }
>(
  'auth/profile',
  async (payload, { rejectWithValue, dispatch, getState }) => {
    const response = await fetch(
      `${import.meta.env.VITE_API_URL}/users/profile`,
      {
        method: 'PUT',
        headers: {
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          Authorization: `Bearer ${getState().auth.token!}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload),
      },
    );

    if (response.status === 401) {
      const error = (await response.json()) as ErrorResponse;
      dispatch(replace('/login?redirect=/profile'));

      return rejectWithValue(error.message);
    }

    return response.json();
  },
  {
    condition: (_, { getState }) => Boolean(getState().auth.token),
  },
);

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    logout: () => ({}),
  },
  extraReducers: (builder) => {
    function buildSuccessState(
      _: AuthState,
      action: PayloadAction<UserWithToken>,
    ) {
      return {
        user: {
          _id: action.payload._id,
          name: action.payload.name,
          email: action.payload.email,
          isAdmin: action.payload.isAdmin,
        },
        token: action.payload.token,
      };
    }

    function buildErrorState(
      _: AuthState,
      action: PayloadAction<
        string | undefined,
        string,
        {
          rejectedWithValue: boolean;
        },
        SerializedError
      >,
    ) {
      if (action.meta.rejectedWithValue)
        return {
          error: action.payload,
        };

      if (action.error)
        return {
          error: action.error.message,
        };
    }

    builder.addCase(login.fulfilled, buildSuccessState);
    builder.addCase(register.fulfilled, buildSuccessState);
    builder.addCase(updateProfile.fulfilled, buildSuccessState);

    builder.addCase(login.rejected, buildErrorState);
    builder.addCase(register.rejected, buildErrorState);
    builder.addCase(updateProfile.rejected, (state, action) => {
      state.error = action.payload ?? action.error.message;
    });
  },
});

export const { logout } = authSlice.actions;

export const userSelector = (state: AppState) => state.auth.user;

export default authSlice.reducer;
