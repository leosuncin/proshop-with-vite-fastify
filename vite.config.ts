import reactRefresh from '@vitejs/plugin-react-refresh';
import { defineConfig } from 'vite';
import tsChecker from 'vite-plugin-ts-checker';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [reactRefresh(), tsChecker()],
});
