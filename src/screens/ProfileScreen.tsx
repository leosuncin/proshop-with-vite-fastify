import React, { useEffect } from 'react';
import { Alert, Button, Col, Form, Row } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import type { RouteComponentProps } from 'react-router-dom';

import { useAppDispatch, useAppSelector } from '../app/hooks';
import {
  ProfileBody,
  updateProfile,
  userSelector,
} from '../app/slices/authSlice';
import Loader from '../components/Loader';

interface ProfileData extends ProfileBody {
  confirmPassword: string;
}

export const validations = {
  name: {
    minLength: {
      value: 1,
      message: 'Full name too short (at least 1 characters required)',
    },
  },
  email: {
    pattern: {
      message: 'Email should be a valid address',
      value:
        /^[\w.!#$%&'*+\\/=?^`{|}~-]+@[a-zA-Z\d](?:[a-zA-Z\d-]{0,61}[a-zA-Z\d])?(?:\.[a-zA-Z\d](?:[a-zA-Z\d-]{0,61}[a-zA-Z\d])?)*$/,
    },
  },
  password: {
    minLength: {
      message: 'Password too short (at least 8 characters required)',
      value: 8,
    },
  },
  confirmPassword: {
    minLength: {
      message: 'Confirm password too short (at least 8 characters required)',
      value: 8,
    },
  },
} as const;

function ProfileScreen({ history }: RouteComponentProps) {
  const dispatch = useAppDispatch();
  const user = useAppSelector(userSelector);
  const error = useAppSelector((state) => state.auth.error);
  const { handleSubmit, register, formState, reset, watch } =
    useForm<ProfileData>({
      defaultValues: user,
    });

  useEffect(() => {
    if (!user) history.push('/login?redirect=/profile');
  }, [user, history]);

  return (
    <Row>
      <Col md={3}>
        <h2>User Profile</h2>

        {error && <Alert variant="danger">{error}</Alert>}

        {formState.isSubmitSuccessful && !error && (
          <Alert variant="success">Profile Updated</Alert>
        )}

        {formState.isSubmitting && <Loader />}

        <Form
          onSubmit={handleSubmit((data) => dispatch(updateProfile(data)))}
          onReset={(event) => {
            event.preventDefault();
            reset();
          }}
        >
          <Form.Group controlId="profile-name">
            <Form.Label>Full name</Form.Label>
            <Form.Control
              type="name"
              placeholder="Enter name"
              autoComplete="name"
              {...register('name', validations.name)}
            />
            {formState.errors.name && (
              <Form.Text className="text-warning">
                {formState.errors.name.message}
              </Form.Text>
            )}
          </Form.Group>

          <Form.Group controlId="profile-email">
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              autoComplete="email"
              {...register('email', validations.email)}
            />
          </Form.Group>

          <Form.Group controlId="profile-password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Enter password"
              autoComplete="new-password"
              {...register('password', validations.password)}
            />
            {formState.errors.password && (
              <Form.Text className="text-warning">
                {formState.errors.password.message}
              </Form.Text>
            )}
          </Form.Group>

          <Form.Group controlId="profile-confirmPassword">
            <Form.Label>Confirm password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Confirm password"
              autoComplete="new-password"
              {...register('confirmPassword', {
                ...validations.confirmPassword,
                validate: (value) =>
                  value === watch('password')
                    ? true
                    : 'Repeat the same password to confirm',
              })}
            />
            {formState.errors.confirmPassword && (
              <Form.Text className="text-warning">
                {formState.errors.confirmPassword.message}
              </Form.Text>
            )}
          </Form.Group>

          <Button
            type="submit"
            variant="primary"
            disabled={formState.isSubmitting}
          >
            Update
          </Button>
          <Button
            type="reset"
            variant="secondary"
            style={{ marginLeft: '0.5rem' }}
          >
            Reset
          </Button>
        </Form>
      </Col>

      <Col md={9}>
        <h2>My Orders</h2>
      </Col>
    </Row>
  );
}

export default ProfileScreen;
