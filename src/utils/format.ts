export const currencyFormatter = new Intl.NumberFormat(['en', 'es-SV'], {
  style: 'currency',
  currency: 'USD',
});
