import {
  PayloadAction,
  createEntityAdapter,
  createSlice,
} from '@reduxjs/toolkit';

import type { Product } from '../../products';
import type { AppState } from '../store';

export interface CartItem {
  product: Product['_id'];
  name: string;
  image: string;
  price: number;
  countInStock: number;
  quantity: number;
}

export interface ShippingAddress {
  address: string;
  postalCode: string;
  city: string;
  country: string;
}

export type CartState = typeof initialState;

const cartItemsAdapter = createEntityAdapter<CartItem>({
  selectId: (item) => item.product,
});

const initialState = cartItemsAdapter.getInitialState<{
  shippingAddress?: ShippingAddress;
  paymentMethod?: string;
}>({
  shippingAddress: undefined,
  paymentMethod: undefined,
});

function isCartItem(item: Product | CartItem): item is CartItem {
  return 'product' in item && typeof item.product === 'string';
}

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addItem: {
      prepare: (args: { quantity: number; product: Product | CartItem }) => ({
        payload: {
          product: isCartItem(args.product)
            ? args.product.product
            : args.product._id,
          name: args.product.name,
          image: args.product.image,
          price: args.product.price,
          countInStock: args.product.countInStock,
          quantity: args.quantity,
        },
      }),
      reducer: cartItemsAdapter.upsertOne,
    },
    removeItem: cartItemsAdapter.removeOne,
    setShippingAddress: (state, action: PayloadAction<ShippingAddress>) => {
      state.shippingAddress = action.payload;
    },
    setPaymentMethod: (state, action: PayloadAction<string>) => {
      state.paymentMethod = action.payload;
    },
  },
});

export const { addItem, removeItem, setShippingAddress, setPaymentMethod } =
  cartSlice.actions;

export const itemsSelector = cartItemsAdapter.getSelectors(
  (rootState: AppState) => rootState.cart,
).selectAll;

export const countItemsSelector = (rootState: AppState) =>
  itemsSelector(rootState).reduce((acc, item) => acc + item.quantity, 0);

export default cartSlice.reducer;
