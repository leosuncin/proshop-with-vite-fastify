import { faStar as farStar } from '@fortawesome/free-regular-svg-icons';
import { faStar, faStarHalfAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

type RatingProps = {
  value: number;
  text: string;
  color?: string;
};

function Rating({ value, text, color = '#f8e825' }: RatingProps) {
  return (
    <span className="rating">
      <FontAwesomeIcon
        icon={value >= 1 ? faStar : value >= 0.5 ? faStarHalfAlt : farStar}
        color={color}
      />
      <FontAwesomeIcon
        icon={value >= 2 ? faStar : value >= 1.5 ? faStarHalfAlt : farStar}
        color={color}
      />
      <FontAwesomeIcon
        icon={value >= 3 ? faStar : value >= 2.5 ? faStarHalfAlt : farStar}
        color={color}
      />
      <FontAwesomeIcon
        icon={value >= 4 ? faStar : value >= 3.5 ? faStarHalfAlt : farStar}
        color={color}
      />
      <FontAwesomeIcon
        icon={value >= 5 ? faStar : value >= 4.5 ? faStarHalfAlt : farStar}
        color={color}
      />
      <span>{text}</span>
    </span>
  );
}

export default Rating;
