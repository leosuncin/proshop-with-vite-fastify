import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import {
  Button,
  Col,
  FormControl,
  Image,
  ListGroupItem,
  Row,
} from 'react-bootstrap';
import { Link } from 'react-router-dom';

import { useAppDispatch } from '../app/hooks';
import { CartItem as Item, addItem, removeItem } from '../app/slices/cartSlice';
import { currencyFormatter } from '../utils/format';

export type CartItemProps = {
  item: Item;
};

function CartItem({ item }: CartItemProps) {
  const dispatch = useAppDispatch();

  return (
    <ListGroupItem>
      <Row>
        <Col md={2}>
          <Image src={item.image} alt={item.name} fluid rounded />
        </Col>

        <Col md={3}>
          <Link to={`/product/${item.product}`}>
            <a className="h4">{item.name}</a>
          </Link>
        </Col>

        <Col md={2}>{currencyFormatter.format(item.price)}</Col>

        <Col md={2}>
          <FormControl
            as="select"
            aria-label="Quantity"
            value={item.quantity}
            onChange={(event) => {
              dispatch(
                addItem({
                  product: item,
                  quantity: Number(event.target.value),
                }),
              );
            }}
          >
            {Array.from({ length: item.countInStock }, (_, x) => (
              <option key={x + 1} value={x + 1}>
                {x + 1}
              </option>
            ))}
          </FormControl>
        </Col>

        <Col md={2}>
          <Button
            type="button"
            variant="light"
            aria-label="Remove item from shopping cart"
            onClick={() => {
              dispatch(removeItem(item.product));
            }}
          >
            <FontAwesomeIcon icon={faTrash} />
          </Button>
        </Col>
      </Row>
    </ListGroupItem>
  );
}

export default CartItem;
