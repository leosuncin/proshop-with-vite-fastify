import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

import type { Product } from '../../products';

interface ProductsResponse {
  products: Product[];
}

export const productsApi = createApi({
  reducerPath: 'products',
  baseQuery: fetchBaseQuery({ baseUrl: import.meta.env.VITE_API_URL }),
  tagTypes: ['Product'],
  endpoints: (builder) => ({
    getProducts: builder.query<Product[], void>({
      query: () => '/products',
      transformResponse: (response: ProductsResponse) => response.products,
      providesTags: (products) =>
        products
          ? [
              ...products.map(({ _id: id }) => ({
                type: 'Product' as const,
                id,
              })),
              'Product',
            ]
          : ['Product'],
    }),
    getProductById: builder.query<Product, Product['_id']>({
      query: (id) => `/products/${id}`,
    }),
  }),
  refetchOnFocus: true,
  refetchOnReconnect: true,
});

export const { useGetProductsQuery, useGetProductByIdQuery } = productsApi;
