import {
  Action,
  AnyAction,
  ThunkAction,
  ThunkDispatch,
  configureStore,
} from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { History, createBrowserHistory } from 'history';
import {
  FLUSH,
  PAUSE,
  PERSIST,
  PURGE,
  PersistConfig,
  REGISTER,
  REHYDRATE,
  persistCombineReducers,
  persistStore,
} from 'redux-persist';
import storage from 'redux-persist/es/storage';

import { productsApi } from './services/productsApi';
import authReducer from './slices/authSlice';
import cartReducer from './slices/cartSlice';

export const browserHistory = createBrowserHistory();

const persistConfig: PersistConfig<AppState> = {
  key: 'proshop',
  storage,
  whitelist: ['cart', 'auth'],
};

export function makeStore(history: History = browserHistory) {
  return configureStore({
    reducer: persistCombineReducers(persistConfig, {
      [productsApi.reducerPath]: productsApi.reducer,
      cart: cartReducer,
      auth: authReducer,
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      router: connectRouter(history)!,
    }),
    middleware: (getDefaultMiddleware) => [
      ...getDefaultMiddleware({
        serializableCheck: {
          ignoredActions: [FLUSH, PAUSE, PERSIST, PURGE, REGISTER, REHYDRATE],
        },
      }),
      routerMiddleware(history),
      productsApi.middleware,
    ],
  });
}

const store = makeStore();

export const persistor = persistStore(store);

setupListeners(store.dispatch);

export type AppState = {
  [productsApi.reducerPath]: ReturnType<typeof productsApi.reducer>;
  cart: ReturnType<typeof cartReducer>;
  auth: ReturnType<typeof authReducer>;
  router: ReturnType<ReturnType<typeof connectRouter>>;
};

export type AppDispatch = ThunkDispatch<AppState, null, AnyAction> &
  ThunkDispatch<AppState, undefined, AnyAction> &
  ThunkDispatch<AppDispatch, ReturnType<typeof routerMiddleware>, AnyAction>;

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  AppState,
  unknown,
  Action<string>
>;

export default store;
