import React from 'react';
import { Alert, Button, Col, Form, Row } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';

import { useAppDispatch, useAppSelector } from '../app/hooks';
import {
  RegisterBody,
  register as registerAction,
} from '../app/slices/authSlice';
import FormContainer from '../components/FormContainer';
import Loader from '../components/Loader';

interface RegisterData extends RegisterBody {
  confirmPassword: string;
}

export const validations = {
  name: {
    required: 'Full name should not be empty',
  },
  email: {
    required: 'Email should not be empty',
    pattern: {
      message: 'Email should be a valid address',
      value:
        /^[\w.!#$%&'*+\\/=?^`{|}~-]+@[a-zA-Z\d](?:[a-zA-Z\d-]{0,61}[a-zA-Z\d])?(?:\.[a-zA-Z\d](?:[a-zA-Z\d-]{0,61}[a-zA-Z\d])?)*$/,
    },
  },
  password: {
    required: 'Password should not be empty',
    minLength: {
      message: 'Password too short (at least 8 characters required)',
      value: 8,
    },
  },
  confirmPassword: {
    required: 'Confirm password should not be empty',
    minLength: {
      message: 'Confirm password too short (at least 8 characters required)',
      value: 8,
    },
  },
};

function RegisterScreen() {
  const { handleSubmit, register, formState, watch } = useForm<RegisterData>();
  const dispatch = useAppDispatch();
  const errorMessage = useAppSelector((state) => state.auth.error);
  const search = new URLSearchParams(location.search);
  const redirect = search.get('redirect') ?? '/';

  return (
    <FormContainer>
      <h1>Sign up</h1>
      {errorMessage && <Alert variant="danger">{errorMessage}</Alert>}

      {formState.isSubmitting && <Loader />}

      <Form onSubmit={handleSubmit((data) => dispatch(registerAction(data)))}>
        <Form.Group controlId="register-name">
          <Form.Label>Full name</Form.Label>
          <Form.Control
            placeholder="Enter name"
            autoComplete="name"
            autoFocus
            {...register('name', validations.name)}
          />
          {formState.errors.name && (
            <Form.Text className="text-warning">
              {formState.errors.name.message}
            </Form.Text>
          )}
        </Form.Group>

        <Form.Group controlId="register-email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            autoComplete="email"
            {...register('email', validations.email)}
          />
          {formState.errors.email && (
            <Form.Text className="text-warning">
              {formState.errors.email.message}
            </Form.Text>
          )}
        </Form.Group>

        <Form.Group controlId="register-password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Enter password"
            autoComplete="new-password"
            {...register('password', validations.password)}
          />
          {formState.errors.password && (
            <Form.Text className="text-warning">
              {formState.errors.password.message}
            </Form.Text>
          )}
        </Form.Group>

        <Form.Group controlId="register-confirmPassword">
          <Form.Label>Confirm password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Confirm password"
            autoComplete="new-password"
            {...register('confirmPassword', {
              ...validations.confirmPassword,
              validate: (value) =>
                value === watch('password')
                  ? true
                  : 'Repeat the same password to confirm',
            })}
          />
          {formState.errors.confirmPassword && (
            <Form.Text className="text-warning">
              {formState.errors.confirmPassword.message}
            </Form.Text>
          )}
        </Form.Group>

        <Button
          type="submit"
          variant="primary"
          disabled={formState.isSubmitting}
        >
          Register
        </Button>
      </Form>

      <Row className="py-3">
        <Col>
          Have an account?{' '}
          <Link to={redirect ? `/login?redirect=${redirect}` : '/login'}>
            Login
          </Link>
        </Col>
      </Row>
    </FormContainer>
  );
}

export default RegisterScreen;
