import { faArrowLeft, faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useRef, useState } from 'react';
import {
  Alert,
  Button,
  Card,
  Col,
  Form,
  Image,
  ListGroup,
  Row,
  Toast,
} from 'react-bootstrap';
import { Link, RouteComponentProps } from 'react-router-dom';

import { useAppDispatch } from '../app/hooks';
import { useGetProductByIdQuery } from '../app/services/productsApi';
import { addItem } from '../app/slices/cartSlice';
import Loader from '../components/Loader';
import Rating from '../components/Rating';
import { currencyFormatter } from '../utils/format';

function ProductScreen({ match }: RouteComponentProps<{ id: string }>) {
  const { data: product, isLoading } = useGetProductByIdQuery(match.params.id);
  const dispatch = useAppDispatch();
  const quantityInput = useRef<HTMLSelectElement>(null);
  const [showToast, setShowToast] = useState(false);

  function addToCartHandler() {
    dispatch(
      addItem({
        quantity: Number(quantityInput.current?.value),
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        product: product!,
      }),
    );
    setShowToast(true);
  }

  if (isLoading) return <Loader />;

  if (!product) return <Alert variant="danger">Product not found</Alert>;

  return (
    <>
      <Row>
        <Col
          aria-live="polite"
          aria-atomic="true"
          style={{
            position: 'relative',
          }}
        >
          <Link className="btn btn-light my-3" to="/">
            <FontAwesomeIcon icon={faArrowLeft} /> Go Back
          </Link>
          <Toast
            onClose={() => {
              setShowToast(false);
            }}
            show={showToast}
            delay={3000}
            style={{
              position: 'absolute',
              top: 0,
              right: 0,
            }}
            autohide
          >
            <Toast.Header>
              <strong className="mr-auto">
                <FontAwesomeIcon icon={faShoppingCart} /> Product added to{' '}
                <Link to="/cart">your shopping cart</Link>
              </strong>
            </Toast.Header>
          </Toast>
        </Col>
      </Row>
      <Row>
        <Col md={6}>
          <Image src={product.image} alt={product.name} fluid />
        </Col>

        <Col md={3}>
          <ListGroup variant="flush">
            <ListGroup.Item>
              <h3>{product.name}</h3>
            </ListGroup.Item>

            <ListGroup.Item>
              <Rating
                value={product.rating}
                text={`${product.numReviews} reviews`}
              />
            </ListGroup.Item>

            <ListGroup.Item>Price: ${product.price}</ListGroup.Item>

            <ListGroup.Item>Description: {product.description}</ListGroup.Item>
          </ListGroup>
        </Col>

        <Col md={3}>
          <Card>
            <ListGroup variant="flush">
              <ListGroup.Item>
                <Row>
                  <Col>Price:</Col>
                  <Col>
                    <strong>{currencyFormatter.format(product.price)}</strong>
                  </Col>
                </Row>
              </ListGroup.Item>

              <ListGroup.Item>
                <Row>
                  <Col>Status:</Col>
                  <Col>
                    {product.countInStock > 0 ? 'In Stock' : 'Out Of Stock'}
                  </Col>
                </Row>
              </ListGroup.Item>

              {product.countInStock > 0 ? (
                <ListGroup.Item>
                  <Row>
                    <Col as="label" htmlFor="quantity-input">
                      Quantity:
                    </Col>
                    <Col>
                      <Form.Control
                        id="quantity-input"
                        as="select"
                        ref={quantityInput}
                      >
                        {Array.from(
                          { length: product.countInStock },
                          (_, x) => (
                            <option key={x + 1} value={x + 1}>
                              {x + 1}
                            </option>
                          ),
                        )}
                      </Form.Control>
                    </Col>
                  </Row>
                </ListGroup.Item>
              ) : null}

              <ListGroup.Item>
                <Button
                  className="btn-block"
                  type="button"
                  disabled={product.countInStock === 0}
                  block
                  onClick={addToCartHandler}
                >
                  Add To Cart
                </Button>
              </ListGroup.Item>
            </ListGroup>
          </Card>
        </Col>
      </Row>
    </>
  );
}

export default ProductScreen;
