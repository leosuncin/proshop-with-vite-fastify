import './index.css';

import { ConnectedRouter } from 'connected-react-router';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';

import App from './App';
import store, { browserHistory, persistor } from './app/store';
import Loader from './components/Loader';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate persistor={persistor} loading={<Loader />}>
        <Router>
          <ConnectedRouter history={browserHistory}>
            <App />
          </ConnectedRouter>
        </Router>
      </PersistGate>
    </Provider>
  </React.StrictMode>,
  document.querySelector('#root'),
);
